1. Install tampermonkey extension (greasemonkey *should* also work)
2. Install latest script version from [here](https://bitbucket.org/tamati25/rolz-enhancer/raw/eb6748ffe76bef75bb2cebeb48d8cbeabe512951/rolzenhancer.user.js) (a prompt should open to install the script)
3. Open rolz chat room
4. Enjoy

Pull requests welcome