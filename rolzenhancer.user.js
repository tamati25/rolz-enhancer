// ==UserScript==
// @name         Rolz enhancer
// @namespace    http://127.0.0.1
// @version      0.3.2
// @description  Various enhancements for rolz chat rooms
// @author       tama
// @include      https://rolz.org/dr?room=*
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @grant        unsafeWindow
// @require      https://code.jquery.com/jquery-latest.js
// @require      https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/rangy/1.3.0/rangy-core.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/hallo.js/1.1.1/hallo.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js
// @resource     jqueryui https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css
// @resource     fontawesome https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css
// ==/UserScript==
/* jshint -W097 */
//'use strict';
/* --- injection css --- */
GM_getResourceText('jqueryui');
GM_getResourceText('fontawesome');
GM_addStyle('h2#view { margin: 0 auto; font-size: 150%; color: red; }');
GM_addStyle('.file { float: left; padding: 5px; min-height: 50px; width: 200px; margin: 10px; border: 2px solid black; background-color: #FFFFFF; opacity: 0.75; position: absolute; }');
GM_addStyle('.file .footer { font-size: 70%; color: #CCCCCC; border-top: 1px solid #EFEFEF; padding: 2px; margin-top: 10px; }');
GM_addStyle('#typing { position: absolute; bottom: 60px; left: 15px; font-size: 100%; color: black;}');
GM_addStyle('#dr-panes > div { color: black; }');
GM_addStyle('#pane-x-1 { background: rgba(255, 255, 255, 0.6) !important; }');
GM_addStyle('body { background: rgba(0, 0, 0, 0) url("https://i.imgur.com/f8R3lMF.jpg") repeat scroll 0 0}');
GM_addStyle('#my_nick { width: 5%; color: white; }');
GM_addStyle('#prompt-line { width: 800px; background: none; }');
GM_addStyle('#wiki { position: absolute; right: 40px; text-align: right; }');
GM_addStyle('#logs { position: absolute; right: 80px; text-align: right; }');
GM_addStyle('#wiki_contents, #log_contents { position: absolute; top: 20px; right: 40px; width: 800px; }');
GM_addStyle('#log_contents {  height: 800px; overflow: auto; padding: 10px; background-color: rgba(255, 255, 255, 1); }')

/* --- Configuration par défaut --- */
var server_url = 'wss://colorgame-tamaapps.rhcloud.com:8443/ws';
/* --- Global variables --- */
var me = '';
var ws = null;
var my_uid = '';
var adm = false;
//Draggable div
var $dragging = null;

var last_page = -1;

/* --- Default message handlers --- */
var messageHandler = function (msg)
{
  if (me != '' && msg.type == 'srvmsg')
  {
    //changed nick ?
    var newnickptn = /(.*) is now known as (.*)/;
    var match = newnickptn.exec(msg.text);
    if (match != null)
    {
      if (match[1] == me)
      {
        me = match[2];
        console.debug('New nickname => ' + me);
        //Relance une connexion
        if (ws != null)
        {
          ws.close();
          ws = null;
          my_uid = '';
          $('#typing').text('');
        }
        ws_connect(me);
      }
    }
  }
  console.debug('Message handler : ', msg);
};

/* --- Websocket related functions --- */

function ws_connect(nick, retryIfNoNick)
{
  if (nick == '' && retryIfNoNick)
  {
    console.error('Username was not detected, trying again');
    whoami(true);
  }
  if (ws == null)
  {
    ws = new WebSocket(server_url);
    //Set handlers for websocket messages
    ws.onopen = function ()
    {
      console.debug('Connected, yay~~');
      ws.send('- connect ' + me);
      $('#board').empty();
    };

    ws.onmessage = function (event)
    {
      var what = event.data;
      var sections = what.split('|');
      var action = sections[0];
      var data = sections.slice(1, sections.length).join('|');
      if (action == 'uid' && my_uid == '')
      {
        console.debug('connect');
        my_uid = sections[1];
        var admin = sections[2];
        if (admin == 'ADM')
        {
          $('#view').text('Admin');
          adm = true;
        } 
        else
        {
          $('#view').text('User');
        }        //Obtient la liste des box à afficher

        ws.send(my_uid + ' ' + 'LIST');
      }
      else if (action == 'list')
      {
        $('#board').append(atob(data));

        $("body").on('mousedown', '.file', function (e) {
          var target = $(e.target);
          if ($(target[0]).hasClass('file'))
          {
            $dragging = $(e.target);
          }
        });
        $("body").on('mouseup', function (e) {
          $dragging = null;
          var target = $(e.target);
          if ($(target[0]).hasClass('file'))
          {
            var name = $(e.target).attr("id");
            var pos = $(e.target).position();

            console.log(pos);

            Cookies.remove(name);
            Cookies.set(name, encodeURIComponent(pos.left + "," + pos.top), { expires: 365 });
          }
        });

        //Toggle show/hide
        $('.file').on('focus', '[contenteditable]', function () {
          var $this = $(this);
          $this.data('before', $this.html());
          return $this;
        }).on('blur paste', '[contenteditable]', function () {
          var $this = $(this);
          if ($this.data('before') !== $this.html()) {
            $this.data('before', $this);
            var id = $this[0].getAttribute('id');
            var content = btoa($this.html());
            //Content edited, trigger an update on server
            ws.send(my_uid + ' UPDT ' + id + ' ' + content);
          }
          return $this;
        });
        $('.content[contenteditable]').hallo({
          plugins:
          {
            halloformat: {
              'formattings': {
                'bold': true,
                'italic': true,
                'strikethrough': true,
                'underline': false
              }
            },
            halloheadings: {
              'formatBlocks': [
                'p',
                'h2',
                'h3'
              ]
            },
            hallojustify: {
            },
            hallolists: {
              'lists': {
                'ordered': true,
                'unordered': true
              }
            },
            halloreundo: {
            },
            halloimage: {
            }
          },
          toolbar: 'halloToolbarContextual'
        });

        $(".file").css("position", "absolute");
        $(".file").css("left", "0px");
        $(".file").css("top", "0px");

        if (document.cookie == "")
        {
          $(".file").animate({'left': '820px'}, 500);

          var fiches = $(".file");
          for (i = 0; i < fiches.length; ++i)
          {
            var pos = $(fiches[i]).position();
            var name = $(fiches[i]).attr("id");
            
            console.log(name);
            Cookies.set(name, encodeURIComponent((pos.left + 820) + "," + pos.top), { expires: 365 });
          }
          console.log(document.cookie);
        }
        else
        {
          cookies = Cookies.get();
          Object.keys(cookies).forEach(function(key, index) {
            var pos = Cookies.get(key);
            var x = pos.split(encodeURIComponent(","))[0];
            var y = pos.split(encodeURIComponent(","))[1];
			console.log(x, y);
            $("#" + key).animate({left: x + 'px', top: y + 'px'});
          });
          console.log(cookies);
        }
        //$(".file").css("position", "relative");
		$(".content").css("position", "relative");
      }
      else if (action == 'refresh')
      {
        var target = sections[1];
        var data = sections.slice(2, sections.length).join('|');
        console.debug('refresh ', target, data);
        $('.content[id=\'' + target + '\']').html(atob(data));
      }
      else if (action == 'type')
      {
        $('#typing').text(data);
      }
      else if (action == 'bg')
      {
          console.debug(data);
          $("body").css("background", "rgba(0, 0, 0, 0) url(\"" + data + "\") repeat scroll 0 0");
      }
    };
  }
}/* --- Interface --- */
function load_page() {
  last_page = last_page + 1;

  $.post('https://rolz.org/dr/tabs/log-page', { room: ':5512', page: last_page }, function(data) {
      for (var i = data.items.length - 1; i >= 0; i--) {
        var item = data.items[i];

        if (item.type == "txtmsg") {
          //prepend : texte "à l'envers"
          $("#log_txt").prepend("<p style=\"" + item.style + "\">" + item.text + "</p>");
          $("#log_txt").prepend("<p><strong>" + item.from + " (" + item.h_time + ")" + "</strong></p>");
        } else if (item.type == "dicemsg") {
          $("#log_txt").prepend("<p><strong>[" + item.h_time + "] " + item.from + " rolled " + item.input + " => " + item.details + " = " + item.result + "</strong></p>");
        }
      }
    });
}

function load_ui()
{
  $("#content").append("<div id=\"tab-poke\"><div id=\"board\"></div></div>");
  $('.tab-pane').hide();
  $('.btn').removeClass('active');
  //$('#context').append('<div id="tab-poke" class="tab-pane"><p><strong>Poke v0.3 (06/11/2015)</strong></p><p>Server address : <input type="text" id="server_url" name="ws_server_url" /></p><h2 id="view"></h5><div id="board"></div></div>');
  $('#content').append('<div id="typing"></div>');
  $('#tab-poke').show();
  $('#tbtn-toto').addClass('active');
  $('#server_url').val('[not implemented yet]');
  $('#server_url').attr('disabled', 'disabled');

  $("#topmenu a").remove();
  $("#topmenu").append("<a id=\"wiki\" class=\"site-menu\" href=\"#\">Wiki</a>");
  $("#topmenu").append("<a id=\"logs\" class=\"site-menu\" href=\"#\">Logs</a>");
  $("#content").append("<div id=\"wiki_contents\"><iframe style=\"width: 100%; height: 800px; border: medium none;\" src=\"/wiki/inframe?room=:5512\" id=\"wiki_frame\"></iframe></div>");
  $("#content").append("<div id=\"log_contents\"><p><a href=\"#\" class=\"morelog\">-- Plus ancien --</a></p><div id=\"log_txt\"></div></div>");
  $("#wiki_contents").hide();
  $("#log_contents").hide();
  
  //Hack, wait 2s to get the player nick and then connect to the websocket
  setTimeout(function() { 
    me = $("#my_nick").text();
    ws_connect(me, true); }, 2000);
  
  $("#wiki").click(function() {
   $("#log_contents").hide();
   $("#wiki_contents").toggle();  
  });

  $("#logs").click(function() {
    $("#wiki_contents").hide();
    $("#log_contents").toggle();

    if (last_page == -1) {
      load_page();
    }
  });

  $("a.morelog").click(function() {
    load_page();
  })
}// Your code here...

$(document).ready(function () {
  $('span#tab-bar').append('<a class="btn" id="tbtn-toto" title="UI"><i class="fa fa-plus"></i></a>');
  /*$('#tbtn-toto').click(function () {
    load_ui();
  });*/
  $(document.body).on('mousemove', function (e) {
    if ($dragging) {
      $dragging.offset({
        top: e.pageY,
        left: e.pageX
      });
    }
  });
  $('#context').css({
    position: 'relative'
  });
  $('#prompt-element').on('keyup', function (e) {
    if (my_uid != '' && ws != null)
    {
      var text_length = $('#prompt-element').val().length;
      var ws_msg = my_uid + ' ' + 'TYPE' + ' ' + text_length;
      //console.log('=> ', ws_msg);
      ws.send(ws_msg);
      //console.log("Pressed key, current value is ", $(this).val());
    }
  });
  $("#pane-x-2").fadeOut();
  $(".h3-panel").fadeOut();
  $("#pane-x-1").css("position", "absolute");
  $("#pane-x-1").css("left", "12px");
  $("#tab-poke").css("position", "absolute");
  $("#tab-poke").css("left", "810px");
  $("#tab-poke").css("width", "calc(100% - 820px)");
  load_ui();
});
/* --- Utils --- */
function send(msg) {
  $('#prompt-element').val(msg);
  $('#send-button').click();
}

function sendBotMsg(msg)
{
    $("#output.dr-pane-content").append("<div class=\"chline txtmsg  ctx_\"><div class=\"line-icon\"><a href=\"/info?X18086117\" target=\"_blank\" data-title=\"2016-12-13 18:58 UTC\"><img src=\"/img/d6-32px.png\" width=\"16\" style=\"opacity:0.5\" align=\"absmiddle\"></a></div><div class=\"line-content\"><span class=\"username\" style=\"color:red\">Echo bot</span>:<div class=\"msg-content\">" + msg + "</div></div></div>");
}

sendLineOld = unsafeWindow.sendLine;
function newSendLine(contentLine)
{
    var contentToSend = $("#prompt-element").val();

    if (contentToSend !== '')
    {
        contentLine = contentToSend;
    }
    console.debug("sendLine(" + contentToSend + ")");

    if (contentLine === undefined || contentLine === '')
    {
        return;
    }

    var postToServer = true;

    var sections = contentLine.split(' ');
    if (sections[0] === '@EchoBot')
    {
        var what = sections[1];
        if (what === 'hello')
        {
            sendBotMsg("Hello world.");
        }
        else if (what === 'request')
        {
            nb_dice = parseInt(sections[2], 10);
            max = parseInt(sections[3], 10);
            diff = parseInt(sections[4], 10);
            sendBotMsg("<div class=\"dice_throw\">Requested dice throw : [" + nb_dice + "] x [ " + max + " ] with diff " + diff + "</div>");
        }
        else if (what === 'dialog')
        {
            sendBotMsg("<div class=\"dialog_txt\">This is a dialog</div><div class=\"dialog_actions\"><button type=\"button\" onclick=\"ok()\">OK</button></div>");
        }
        else if (what === 'background')
        {
            ws.send(my_uid + " BCKG " + sections[2]);
            sendBotMsg("Background change request received");
        }
        else
        {
            sendBotMsg("I didn't understand that, try again.");
        }
        postToServer = false;
    }

    if (postToServer === true)
    {
        sendLineOld(contentLine);
    }
    $("#prompt-element").val("");
};
unsafeWindow.sendLine = exportFunction(newSendLine, unsafeWindow);
